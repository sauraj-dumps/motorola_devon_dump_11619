#
# Copyright (C) 2023 The LineageOS Project
#
# SPDX-License-Identifier: Apache-2.0
#

# Enable updating of APEXes
$(call inherit-product, $(SRC_TARGET_DIR)/product/updatable_apex.mk)

# Include GSI keys
$(call inherit-product, $(SRC_TARGET_DIR)/product/gsi_keys.mk)

# A/B
$(call inherit-product, $(SRC_TARGET_DIR)/product/virtual_ab_ota.mk)

PRODUCT_PACKAGES += \
    android.hardware.boot@1.2-impl \
    android.hardware.boot@1.2-impl.recovery \
    android.hardware.boot@1.2-service

PRODUCT_PACKAGES += \
    update_engine \
    update_engine_sideload \
    update_verifier

PRODUCT_PACKAGES += \
    checkpoint_gc \
    otapreopt_script

# fastbootd
PRODUCT_PACKAGES += \
    android.hardware.fastboot@1.1-impl-mock \
    fastbootd

# Health
PRODUCT_PACKAGES += \
    android.hardware.health@2.1-impl \
    android.hardware.health@2.1-service

# Overlays
PRODUCT_ENFORCE_RRO_TARGETS := *

# Partitions
PRODUCT_BUILD_SUPER_PARTITION := false
PRODUCT_USE_DYNAMIC_PARTITIONS := true

# Product characteristics
PRODUCT_CHARACTERISTICS := default

# Rootdir
PRODUCT_PACKAGES += \
    init.qcom.sensors.sh \
    init.gbmods.sh \
    init.qti.media.sh \
    init.mmi.block_perm.sh \
    init.mdm.sh \
    init.mmi.boot.sh \
    init.qcom.sdio.sh \
    init.class_main.sh \
    init.mmi.shutdown.sh \
    init.mmi.laser.sh \
    init.crda.sh \
    apanic_save.sh \
    pstore_annotate.sh \
    init.qcom.class_core.sh \
    init.qti.dcvs.sh \
    init.qti.qcv.sh \
    init.mmi.mdlog-getlogs.sh \
    qca6234-service.sh \
    init.qcom.post_boot.sh \
    init.oem.hw.sh \
    init.mmi.usb.sh \
    apanic_annotate.sh \
    hardware_revisions.sh \
    init.mmi.wlan-getlogs.sh \
    init.mmi.touch.sh \
    init.qcom.efs.sync.sh \
    init.qti.chg_policy.sh \
    init.qcom.usb.sh \
    init.qti.display_boot.sh \
    apanic_copy.sh \
    init.oem.fingerprint2.sh \
    init.qti.early_init.sh \
    init.qcom.coex.sh \
    init.mmi.modules.sh \
    init.qcom.sh \
    init.qcom.early_boot.sh \

PRODUCT_PACKAGES += \
    fstab.qcom.zram \
    init.mmi.charge_only.rc \
    init.mmi.wlan.rc \
    init.qti.ufs.rc \
    init.vendor.st21nfc.rc \
    init.qcom.rc \
    init.mmi.usb.rc \
    init.mmi.rc \
    init.mmi.diag_mdlog.rc \
    init.target.rc \
    init.mmi.overlay.rc \
    init.qcom.factory.rc \
    init.mmi.debug.rc \
    init.mmi.chipset.rc \
    init.mmi.diag.rc \
    init.qcom.usb.rc \
    init.mmi.tcmd.rc \
    init.recovery.qcom.rc \

PRODUCT_COPY_FILES += \
    $(LOCAL_PATH)/rootdir/etc/fstab.qcom.zram:$(TARGET_VENDOR_RAMDISK_OUT)/first_stage_ramdisk/fstab.qcom.zram

# Shipping API level
PRODUCT_SHIPPING_API_LEVEL := 31

# Soong namespaces
PRODUCT_SOONG_NAMESPACES += \
    $(LOCAL_PATH)

# Inherit the proprietary files
$(call inherit-product, vendor/motorola/devon/devon-vendor.mk)
